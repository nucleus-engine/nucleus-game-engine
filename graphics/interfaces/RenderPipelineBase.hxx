#pragma once


#include <cstdint>
#include "../GraphicsAdapter.hxx"
#include <vector>
#include "../../utils/Typedefs.hxx"
#include <chrono>

namespace Rendering
{
	class RenderPipelineBase
	{
	public:
		RenderPipelineBase() = default;
		virtual ~RenderPipelineBase() = default;
		virtual uint16_t Init(WindowHandle window) = 0;
		virtual void Deinit() = 0;
		virtual std::vector<GraphicsAdapter> GetAdapters() = 0;
		// Window Methods
		virtual void Resize(uint32_t width, uint32_t height) = 0;
		virtual void SetFullScreen(bool fullscreen) = 0;
		virtual void Minimize() = 0;
		virtual bool Initialized() const = 0;
		virtual void SetMinFrametime(std::chrono::microseconds us) = 0;

	protected:
		GraphicsAdapter _adapter;
		bool _isInitialized = false;
		bool _isVSyncActive = false;
		bool _isFullscreen = false;
		bool _isTearingActive = false;
		uint16_t _resWidth = 0;
		uint16_t _resHeight = 0;
	};
}
