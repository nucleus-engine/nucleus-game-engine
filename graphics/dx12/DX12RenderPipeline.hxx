#pragma once

#include <array>
#include <memory>
#include <chrono>
#include <d3dx12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

#include "../interfaces/RenderPipelineBase.hxx"
#include "../GraphicsAdapter.hxx"
#include "MainRenderThread.hxx"
#include "Types.hxx"

using namespace Microsoft::WRL;
using namespace Rendering::DirectX12::Types;

namespace Rendering::DirectX12 {
	constexpr bool USE_SUPPORT_GPU = true;
	constexpr uint16_t BUFF_SIZE = 3;
	constexpr uint8_t CONTXT_SIZE = 3;


	class DX12RenderPipeline : public RenderPipelineBase {
	public:
		DX12RenderPipeline();
		~DX12RenderPipeline() = default;
		uint16_t Init(WindowHandle window) override;
		void LoadAssets();
		void Deinit() override;
		[[nodiscard]] bool Initialized() const override;
		std::vector<Rendering::GraphicsAdapter> GetAdapters() override;

	private:
		// --------------- Windows References --------------------
		WindowHandle _winHandle{};
		std::vector<GraphicsAdapter> _adapters;

		 // Pipeline objects.
		CD3DX12_VIEWPORT _viewport;
		CD3DX12_RECT _scissorRect;
		ComPtr<IDXGISwapChain3> _swapChain;
		ComPtr<D3D12Device> _mainGPU;
		ComPtr<D3D12Device> _supportGPU;
		ComPtr<ID3D12Resource> _renderTargets[BUFF_SIZE];
		ComPtr<ID3D12Resource> _depthStencil;
		ComPtr<ID3D12CommandAllocator> _commandAllocator;
		ComPtr<ID3D12CommandQueue> _commandQueue;
		ComPtr<ID3D12RootSignature> _rootSignature;
		ComPtr<ID3D12DescriptorHeap> _rtvHeap;
		ComPtr<ID3D12DescriptorHeap> _dsvHeap;
		ComPtr<ID3D12DescriptorHeap> _cbvSrvHeap;
		ComPtr<ID3D12DescriptorHeap> _samplerHeap;
		ComPtr<ID3D12PipelineState> _pipelineState;
		ComPtr<ID3D12PipelineState> _pipelineStateShadowMap;

		// App resources.
		D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;
		D3D12_INDEX_BUFFER_VIEW m_indexBufferView;
		ComPtr<ID3D12Resource> m_textures[_countof(SampleAssets::Textures)];
		ComPtr<ID3D12Resource> m_textureUploads[_countof(SampleAssets::Textures)];
		ComPtr<ID3D12Resource> m_indexBuffer;
		ComPtr<ID3D12Resource> m_indexBufferUpload;
		ComPtr<ID3D12Resource> m_vertexBuffer;
		ComPtr<ID3D12Resource> m_vertexBufferUpload;
		UINT m_rtvDescriptorSize;
		int m_titleCount;
		double m_cpuTime;

		// Synchronization objects.
		HANDLE m_workerBeginRenderFrame[CONTXT_SIZE];
		HANDLE m_workerFinishShadowPass[CONTXT_SIZE];
		HANDLE m_workerFinishedRenderFrame[CONTXT_SIZE];
		HANDLE m_threadHandles[CONTXT_SIZE];
		UINT m_frameIndex;
		HANDLE m_fenceEvent;
		ComPtr<ID3D12Fence> m_fence;
		UINT64 m_fenceValue;




			// State values
		uint64_t _numFrames = 0;

		void GetAdaptersInternal();

		void SetDebugModeActive() const;

		void Resize(uint32_t width, uint32_t height) override;

		void SetFullScreen(bool fullscreen) override;

		void Minimize() override;

		void SetMinFrametime(std::chrono::microseconds us) override;

		void LoadPipeline();
	};
}
