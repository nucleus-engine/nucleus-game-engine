#include <future>

#include "MainRenderThread.hxx"
#include "../../utils/Logger.hxx"
#include "DX12RenderPipeline.hxx"

using namespace Rendering::DirectX12::Types;

namespace Rendering::DirectX12::MainRenderThread {
	void Init(D3D12Device* mainGPU, D3D12Device* supportGPU = nullptr) {
		_mainGPU.reset(mainGPU);
		_supportGPU.reset(supportGPU);
	}


	void Start() {
		_thread = std::thread(Update);
	}

	void Stop() {}

	void Pause() {}

	void Forward() {}

	void SetMinFrameTime(std::chrono::microseconds us) {}

	void Update() {
		for(;;) {
			auto start = std::chrono::system_clock::now();
			// Clear the render target.
			{
				CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
					_backBuffer.Get(),
					D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);

				_commandListMgr->Lists()[0]->ResourceBarrier(1, &barrier);
				FLOAT clearColor[] = { 0.4f, 0.6f, 0.9f, 1.0f };
				CD3DX12_CPU_DESCRIPTOR_HANDLE rtv(g_RTVDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
												  g_CurrentBackBufferIndex, g_RTVDescriptorSize);


				g_CommandList->ClearRenderTargetView(rtv, clearColor, 0, nullptr);
			}
			for(int x = 0; x < COMMANDLIST_RECORD_THREADS; ++x) {
				_commandListRecordThreads[x] = std::async(
					std::launch::async,
					&MainRenderThread::RecordCommandLists
				);
			}
			auto resourceFut = std::async(
				std::launch::async,
				&MainRenderThread::CreateResources
			);
			auto posFut = std::async(
				std::launch::async,
				&MainRenderThread::CompilePSOs
			);
			for(int x = 0; x < COMMANDLIST_RECORD_THREADS; ++x) {
				_commandListRecordThreads[x].wait();
			}
			resourceFut.wait();
			posFut.wait();
			for(int x = 0; x < COMMANDLIST_EXECUTE_THREADS; ++x) {
				_commandListExecuteThreads[x] = std::async(
					std::launch::async,
					&MainRenderThread::SubmitCommandLists,
					std::vector<CommandList>()
				);
			}
			for(int x = 0; x < COMMANDLIST_EXECUTE_THREADS; ++x) {
				_commandListExecuteThreads[x].wait();
			}
			for(int x = 0; x < COMMANDLIST_EXECUTE_THREADS; ++x) {
				_commandListExecuteThreads[x] = std::async(
					std::launch::async,
					&MainRenderThread::ExecuteCommandLists,
					std::vector<CommandList>()
				);
			}
			for(int x = 0; x < COMMANDLIST_EXECUTE_THREADS; ++x) {
				_commandListExecuteThreads[x].wait();
			}
			auto end = std::chrono::system_clock::now();
			std::chrono::duration<float, std::micro> delta = end - start;
			Log("Render Thread: " + std::to_string(delta.count() / 1000) + "ms");
		}
	}

	void Clear() {

	}

	void RecordCommandLists() {}

	void CompilePSOs() {}

	void CreateResources() {}

	void SubmitCommandLists(std::vector<CommandList> lists) {}

	void ExecuteCommandLists(std::vector<CommandList> lists) {}

	void ClearThreads() {

	}
}
