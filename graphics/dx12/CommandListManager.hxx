#pragma once
#include <cstdint>
#include <array>
#include "CommandList.hxx"


namespace Rendering::DirectX12 {
	constexpr uint8_t MAX_COMMAND_LISTS = 20;

	class CommandListManager
	{
	public:
		CommandListManager();
		~CommandListManager();

		std::array<CommandList, MAX_COMMAND_LISTS> Lists();

	private:
		std::array<CommandList, MAX_COMMAND_LISTS> _lists;
	};
}
