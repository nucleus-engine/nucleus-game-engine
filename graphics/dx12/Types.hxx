#pragma once
#include <dxgi1_6.h>
#include <dxgi.h>
#include <d3d12.h>

namespace Rendering::DirectX12::Types
{
	// Typedef for D3D and DXGI interface version to make it more verbose which version is used.
	// All typedefs resolve around the lowest Windows 10 version targeted. This currently is 1803
	// Once past Windows 1803 versions share reach 90%, newer versions will be used. Most likely Version 1809 ones.

	typedef ID3D12Device4 D3D12Device;
	typedef IDXGISwapChain4 DXGISwapChain;
	typedef IDXGIFactory2 DXGIFactory;
	typedef IDXGIAdapter1 DXGIAdapter;
}
