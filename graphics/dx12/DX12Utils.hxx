#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <dxgi.h>
#include <dxgi1_6.h>
#include <exception>
#include <vector>
#include "Types.hxx"

using namespace Rendering::DirectX12::Types;
using namespace Microsoft::WRL;

namespace Rendering::DirectX12::Utils {
// From DXSampleHelper.h 
// Source: https://github.com/Microsoft/DirectX-Graphics-Samples
	inline void ThrowIfFailed(HRESULT hr) {
		if(FAILED(hr)) {
			throw std::exception();
		}
	}
// From DXSample.cpp
// Modified example function
// Original Source: https://github.com/Microsoft/DirectX-Graphics-Samples/tree/develop/Samples/UWP/D3D12xGPU
	inline void GetHardwareAdapters(DXGIFactory* pFactory,
									DXGIAdapter** ppAdapter,
									DXGI_GPU_PREFERENCE preferenceOrder = DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE,
									DXGIAdapter* skipAdapter = nullptr) {
		ComPtr<DXGIAdapter> adapter;

		ComPtr<IDXGIFactory6> factory6;
		if(SUCCEEDED(pFactory->QueryInterface(IID_PPV_ARGS(&factory6)))) {
			for(
				UINT adapterIndex = 0;
				DXGI_ERROR_NOT_FOUND != factory6->EnumAdapterByGpuPreference(
					adapterIndex,
					preferenceOrder,
					IID_PPV_ARGS(&adapter)
				);
				++adapterIndex
				) {
				DXGI_ADAPTER_DESC1 desc;
				adapter->GetDesc1(&desc);
				if(desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
					// Don't select the Basic Render Driver adapter.
					// If you want a software adapter, pass in "/warp" on the command line.
					continue;
				}

				// Check to see whether the adapter supports Direct3D 12, but don't create the
				// actual device yet.
				if(SUCCEEDED(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr))) {
					DXGI_ADAPTER_DESC1 desc1Skip;
					
					if(!skipAdapter) { break;; }
					skipAdapter->GetDesc1(&desc1Skip);
					if(desc1Skip.DeviceId == desc.DeviceId) {
						continue;
					}
					break;
				}
			}
		} else {
			for(UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != pFactory->EnumAdapters1(adapterIndex, &adapter); ++adapterIndex) {
				DXGI_ADAPTER_DESC1 desc;
				adapter->GetDesc1(&desc);

				if(desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
					// Don't select the Basic Render Driver adapter.
					// If you want a software adapter, pass in "/warp" on the command line.
					continue;
				}

				// Check to see whether the adapter supports Direct3D 12, but don't create the
				// actual device yet.
				if(SUCCEEDED(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr))) {
					if(skipAdapter == nullptr) { break; }
					
					DXGI_ADAPTER_DESC1 desc1Skip;
					adapter->GetDesc1(&desc);
					
					if(!skipAdapter) { continue; }
					skipAdapter->GetDesc1(&desc1Skip);
					if(desc1Skip.DeviceId == desc.DeviceId) {
						continue;
					}
					break;
				}
			}
		}
		*ppAdapter = adapter.Detach();
	}
}