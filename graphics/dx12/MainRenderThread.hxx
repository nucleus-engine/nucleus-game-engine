#pragma once

#include <array>
#include <thread>
#include <future>
#include <d3dx12.h>

#include "CommandListManager.hxx"
#include "WorkerThread.hxx"
#include "CommandList.hxx"
#include "Types.hxx"

using namespace Microsoft::WRL;
using namespace Rendering::DirectX12::Types;

/// The master render thread that gets all work and executes it via worker threads
namespace Rendering::DirectX12::MainRenderThread {

	/// The number of threads used to gather commandlists and submit them to the master thread
	constexpr uint8_t COMMANDLIST_RECORD_THREADS = 2;
	/// ExecuteCommandList calls should be between 5-10 calls per frame. Since Most system have 4-8 cores, we use 4 Threads.
	constexpr uint8_t COMMANDLIST_EXECUTE_CALLS = 8;
	constexpr uint8_t COMMANDLIST_EXECUTE_THREADS = 4;
	constexpr uint8_t COMMANDLISTS_PER_EXECUTE_THREAD = 2;
	std::thread _thread;

	// Settings
	std::chrono::microseconds _minFrametimeUs;

	uint8_t _commandListsPerExecuteThread;
	uint8_t _numCommandListsExecuteThreads;

	std::unique_ptr<CommandListManager> _commandListMgr;
	ComPtr<ID3D12CommandQueue> _commandQueue;
	ComPtr<DXGISwapChain> _swapChain;


	std::future<void> _psoCompileThread;
	std::future<void> _resourceCreationThread;
	std::array<std::future<void>, COMMANDLIST_RECORD_THREADS> _commandListRecordThreads;
	std::array<std::future<void>, COMMANDLIST_EXECUTE_THREADS> _commandListExecuteThreads;


	/*
	 * Initializes the MainRenderThread
	 */
	void Init(D3D12Device* mainGPU, D3D12Device* supportGPU);

	/*
	 * Starts the render thread
	 */
	void Start();
	/*
	 * Stops the render thread
	 */
	void Stop();
	/*
	 * Pauses the render thread
	 */
	void Pause();
	/*
	 * Forces the render thread to skip all undone render jobs and execute only the latest state
	 */
	void Forward();

	void SetMinFrameTime(std::chrono::microseconds us);
	// The Render Loop
	void Update();
	/*
	 * Clears everything. Should be run at the start of each frame
	 */
	void Clear();
	void RecordCommandLists();
	void CompilePSOs();
	void CreateResources();
	void SubmitCommandLists(std::vector<CommandList> lists);
	void ExecuteCommandLists(std::vector<CommandList> lists);

	void ClearThreads();
}