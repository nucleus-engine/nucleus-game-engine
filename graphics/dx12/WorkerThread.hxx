#pragma once
#include <functional>

namespace Rendering::DirectX12 {

	class WorkerThread {
	public:
		/**
		 * Creates a new instance. Supplied method will be called as a callback when worker thread has data ready
		 */
		WorkerThread(std::function<void(void)> callback);
		~WorkerThread();

		/**
		 * Assigns new work to this worker thread.
		 */
		void GiveWork();
		void RecordCommandLists();
		void CreateResources();
		void CompilePipelineStateObjects();
		/**
		 * Gets the data from this worker when finished 
		 */
		void Pick();
		void Stop();


	private:
		/// Callback that will be executed on the master thread when the worker has data ready 
		std::function<void(void)> _pickupCb;
		/**
		 * Notifies the master thread about this worker being ready to get its data picked
		 */
		void Notify();
	};
}
