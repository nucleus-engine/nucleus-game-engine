#include "CommandListManager.hxx"

namespace Rendering::DirectX12 {
	CommandListManager::CommandListManager() {}

	CommandListManager::~CommandListManager() {}

	std::array<CommandList, MAX_COMMAND_LISTS> CommandListManager::Lists()
	{
		return _lists;
	}

}
