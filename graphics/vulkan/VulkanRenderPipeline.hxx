#pragma once
#include "../interfaces/RenderPipelineBase.hxx"

namespace Rendering::Vulkan
{
	class VulkanRenderPipeline : RenderPipelineBase
	{
	public:
		uint16_t Init();
		void Deinit();
	};
}