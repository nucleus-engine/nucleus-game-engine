#include "RenderPipeline.hxx"
#include "dx12/DX12RenderPipeline.hxx"


namespace Rendering::RenderPipeline {
	RenderPipelineBase* _pipeline = nullptr;
	uint16_t RenderPipeline::Init(Pipeline pipeline, WindowHandle handle) {
		if(pipeline == Pipeline::DirectX12) {
			_pipeline = new DirectX12::DX12RenderPipeline();
			return _pipeline->Init(handle);
		}
		return 0;
	}

	void Deinit() {
		_pipeline->Deinit();
	}

	bool Initialized()
	{
		if(_pipeline == nullptr) return false;
		return _pipeline->Initialized();
	}

	void Resize(uint32_t width, uint32_t height)
	{
		_pipeline->Resize(width, height);
	}

	std::vector<GraphicsAdapter> GetGraphicsAdapters() {
		return _pipeline->GetAdapters();
	}
}
