#pragma once
#include <string>
#include <wrl/client.h>
#include "dx12/DX12Utils.hxx"
#include "./dx12/Types.hxx"

namespace Rendering
{
	typedef struct luid_t {
		DWORD LowPart;
		LONG HighPart;

#ifdef _WIN32
		inline static luid_t FromLUID(_LUID luid)
		{
			return luid_t{ luid.LowPart, luid.HighPart };
		}
#endif
	} luid_t;

	struct GraphicsAdapter
	{
		uint64_t DeviceId = 0;
		luid_t AdapterId;
		uint64_t VendorId = 0;
		uint16_t VRam = 0;
		std::wstring Description;

		// Windows only members
#ifdef _WIN32
		DXGIAdapter* AdapterPtr;
#endif

		bool operator < (const GraphicsAdapter& adapter) const
		{
			return (VRam < adapter.VRam);
		}

		bool operator > (const GraphicsAdapter& adapter) const
		{
			return (VRam > adapter.VRam);
		}

		bool TearingSupport() const
		{
			bool allowsTearing = false;
#ifdef _WIN32
			Microsoft::WRL::ComPtr<IDXGIFactory5> factory5;
			if (SUCCEEDED(CreateDXGIFactory1(IID_PPV_ARGS(&factory5))))
			{
				if (FAILED(factory5->CheckFeatureSupport(
					DXGI_FEATURE_PRESENT_ALLOW_TEARING,
					&allowsTearing, sizeof(allowsTearing))))
				{
					allowsTearing = FALSE;
				}
			}
#endif
			return allowsTearing;
		}
	};
}
