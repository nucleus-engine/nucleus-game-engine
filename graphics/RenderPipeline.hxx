#pragma once
#include <cstdint>
#include "interfaces/RenderPipelineBase.hxx"
#include "GraphicsAdapter.hxx"
#include <chrono>

namespace Rendering::RenderPipeline {
	enum Pipeline {
		DirectX12,
		Vulkan
	};

	extern RenderPipelineBase* _pipeline;

	uint16_t Init(Pipeline pipeline, WindowHandle handle);
	void Deinit();
	std::vector<GraphicsAdapter> GetGraphicsAdapters();
	bool Initialized();
	void Resize(uint32_t width, uint32_t height);
	void Render();
	void SetMinFrameTime(std::chrono::microseconds);
};

