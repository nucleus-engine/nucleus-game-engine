﻿// Defines the entry point for the application. WINDOWS only
//
#include "shared/App.hxx"
#include "app/EngineApp.hxx"
#include <cassert>
#include "graphics/RenderPipeline.hxx"
#include <algorithm>
#include <optional>


constexpr wchar_t* WIN_NAME = L"Nucleus";

Nucleus::App* _app;
WindowHandle _mainWindow;
bool _fullscreen = false;


#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "utils/win32/Win32Utils.hxx"

using namespace Utils::Windows;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int CALLBACK wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow) {
	SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);
	_app = new Nucleus::EngineApp();
	RegisterWindows(hInstance, WndProc);
	auto mainWindow = CreateWindow(Main, WIN_NAME, hInstance, 0);
	_mainWindow = mainWindow;
	ShowWindow(_mainWindow, true);
	_app->Init(hInstance, _mainWindow);
	_app->Start();
	MSG msg = {};
	while(msg.message != WM_QUIT) {
		if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	_app->Stop();
	delete _app;
	return 0;
}

LRESULT CALLBACK WndProc(WindowHandle hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {

	if(Rendering::RenderPipeline::Initialized()) {
		switch(msg) {
			case WM_PAINT:
				_app->Update();
				break;
			case WM_SYSKEYDOWN:
			case WM_KEYDOWN: {
				bool alt = (GetAsyncKeyState(VK_MENU) & 0x8000) != 0;

				if(wParam == VK_RETURN && alt) {
					Utils::Windows::SetFullscreen(_mainWindow, !_fullscreen);
					_fullscreen = !_fullscreen;
				}
				break;
			}
				// The default window procedure will play a system notification sound 
				// when pressing the Alt+Enter keyboard combination if this message is 
				// not handled.
			case WM_SYSCHAR:
				break;
			case WM_SIZE:
			{
				RECT clientRect = {};
				GetWindowRect(hwnd, &clientRect);

				int width = clientRect.right - clientRect.left;
				int height = clientRect.bottom - clientRect.top;

				_app->Resize(width, height);
			}
			break;
			case WM_DESTROY:
				_app->Stop();
				PostQuitMessage(0);
				break;
			default:
				return DefWindowProcW(hwnd, msg, wParam, lParam);
		}
	} else {
		return DefWindowProcW(hwnd, msg, wParam, lParam);
	}

	return 0;
}
#endif