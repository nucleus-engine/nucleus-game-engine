#pragma once

namespace Audio
{
	class AudioManager
	{
		AudioManager() = delete;
		~AudioManager() = delete;

		virtual void PlaySound(void* sound) = 0;
	}; 
}