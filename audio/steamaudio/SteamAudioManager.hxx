#pragma once
#include "../AudioManager.hxx"

namespace Audio::Steamaudio
{
	class SteamAudioManager : public AudioManager
	{
	public:
		SteamAudioManager() = default;
		void PlaySound(void* sound) override;
	};
}