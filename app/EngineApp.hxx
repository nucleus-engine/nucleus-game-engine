#pragma once
#include "../shared/App.hxx"
#include "../graphics/interfaces/RenderPipelineBase.hxx"

namespace Nucleus {
	class EngineApp : public App {
	public:
		EngineApp();
		~EngineApp();
		void Init(AppInstance instance, WindowHandle mainWindow);
		void Start() override;
		void Update() override;
		void Stop() override;
		void SendCommand(void* cmd) override;
		void Resize(uint32_t width, uint32_t height) override;
		void Minimize() override;
		float TimeSinceStartup() override;
		void SetMaxFPS(uint16_t fps) override;
		uint16_t MaxFPS() override;
	private:
		std::chrono::microseconds _minFrameTimeUs;

		void LateUpdate();
		void StaticUpdate();
	};
}
