#include <chrono>

namespace Components {
	class Time {
	public:
		inline static void SetUpdateTime(std::chrono::microseconds us) {
			auto val = us;
		}

		inline static std::chrono::microseconds MinFrameTime()
		{
			return _minFrameTimeUs;
		}

	private:
		inline static float _updateTime;
		inline static float _staticUpdateTime;
		inline static float _lateUpdateTime;
		inline static std::chrono::microseconds _minFrameTimeUs;

		
	};
}