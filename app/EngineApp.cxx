#include "EngineApp.hxx"
#include "../graphics/RenderPipeline.hxx"
#include "../utils/win32/Win32Utils.hxx"
#include <chrono>
#include <string>
#include <cassert>
#include <algorithm>
#include "../graphics/interfaces/RenderPipelineBase.hxx"
#include "../graphics/dx12/DX12RenderPipeline.hxx"
#include "../graphics/RenderPipeline.hxx"
#include "../utils/Logger.hxx"
#include "components/Time.hxx"


namespace Nucleus {
	EngineApp::EngineApp() {

	}

	EngineApp::~EngineApp()
	{
	}

	void EngineApp::Init(AppInstance instance, WindowHandle mainWindow)
	{
		_mainWindow = mainWindow;
		_appInstance = instance;
	}

	void EngineApp::Start() {
		Rendering::RenderPipeline::Init(Rendering::RenderPipeline::DirectX12, _mainWindow);
	}

	void EngineApp::Update() {
		auto start = std::chrono::system_clock::now();
		LateUpdate();
		StaticUpdate();
		auto end = std::chrono::system_clock::now();
		std::chrono::duration<float, std::micro> delta = end - start;
		Log("Game Thread:   " + std::to_string(delta.count() / 1000) + "ms");
	}

	void EngineApp::Stop() {
		Rendering::RenderPipeline::Deinit();
	}

	void EngineApp::SendCommand(void* cmd) {}

	void EngineApp::Resize(uint32_t width, uint32_t height)
	{
		char buff[50];
		sprintf_s(buff, "%i/%i \n", width, height);
		Log(buff);
		Rendering::RenderPipeline::Resize(width, height);
	}

	void EngineApp::Minimize() {}

	float EngineApp::TimeSinceStartup()
	{
		return std::chrono::duration_cast<std::chrono::duration<float>>(_clock.now() - _startup).count();
	}

	void EngineApp::SetMaxFPS(uint16_t fps)
	{
		if(fps < 1)
		{
			fps = 1;
		}
		auto timeS = 1 / fps;
		auto timeUs = static_cast<long>(timeS * (std::pow(10, 7)));
		auto chronoMs = std::chrono::microseconds(timeUs);
	}

	uint16_t EngineApp::MaxFPS()
	{
		return 1.0f / std::chrono::duration_cast<std::chrono::seconds>(_minFrameTimeUs).count();
	}

	void EngineApp::LateUpdate() {}

	void EngineApp::StaticUpdate() {}
}
