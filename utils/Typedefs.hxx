#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
typedef HWND WindowHandle;
typedef HINSTANCE AppInstance;
typedef WNDPROC WindowProcess;
typedef RECT WindowRect;
#endif
