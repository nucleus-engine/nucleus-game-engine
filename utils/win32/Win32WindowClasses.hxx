#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <cassert>
#include <algorithm>
#include <optional>

#undef CreateWindow


namespace Utils::Windows {
	constexpr LPCWSTR MAIN_APPLICATION_WINDOW = L"MAIN_APPLICATION_WINDOW";
	constexpr LPCWSTR DEFAULT_WINDOW = L"DEFAULT_WINDOW";

	enum WindowType {
		Main
	};

	inline LPCWSTR GetClassNameForWindowType(WindowType type) {
		LPCWSTR classStr = L"";
		switch(type) {
			case Main:
				classStr = MAIN_APPLICATION_WINDOW;
				break;
			default:
				classStr = DEFAULT_WINDOW;
		}
		return classStr;
	}

	inline void RegisterMainWindowClass(HINSTANCE hInst,
										WNDPROC winProcess) {
		WNDCLASSEXW windowClass;

		windowClass.cbSize = sizeof(WNDCLASSEX);
		windowClass.style = CS_HREDRAW | CS_VREDRAW;
		windowClass.lpfnWndProc = winProcess;
		windowClass.cbClsExtra = NULL;
		windowClass.cbWndExtra = NULL;
		windowClass.hInstance = hInst;
		windowClass.hIcon = LoadIconA(nullptr, IDI_WINLOGO);
		windowClass.hCursor = LoadCursor(hInst, IDC_ARROW);
		windowClass.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
		windowClass.lpszMenuName = nullptr;
		windowClass.lpszClassName = MAIN_APPLICATION_WINDOW;
		windowClass.hIconSm = LoadIconA(nullptr, IDI_WINLOGO);

		static ATOM atom = RegisterClassExW(&windowClass);
		assert(atom > 0);
	}

	inline WindowHandle CreateWindow(WindowType type,
									 LPCWSTR title,
									 HINSTANCE hInst,
									 DWORD style,
									 WindowRect windowRect = WindowRect { 0, 0, 0, 0 }) {
		const int screenWidth = GetSystemMetrics(SM_CXSCREEN);
		const int screenHeight = GetSystemMetrics(SM_CYSCREEN);

		windowRect.right = windowRect.right == 0 ? screenWidth : windowRect.right;
		windowRect.bottom = windowRect.bottom == 0 ? screenHeight : windowRect.bottom;
		AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

		const int windowWidth = windowRect.right - windowRect.left;
		const int windowHeight = windowRect.bottom - windowRect.top;

		// Center the window within the screen. Clamp to 0, 0 for the top-left corner.
		const int windowX = std::max<int>(0, (screenWidth - windowWidth) / 2);
		const int windowY = std::max<int>(0, (screenHeight - windowHeight) / 2);
		auto mainWindow = CreateWindowExW(
			NULL,
			GetClassNameForWindowType(type),
			title,
			WS_OVERLAPPEDWINDOW,
			windowX,
			windowY,
			windowWidth,
			windowHeight,
			nullptr,
			nullptr,
			hInst,
			nullptr
		);
		assert(mainWindow != nullptr);
		return mainWindow;
	}

	
	inline void RegisterWindows(HINSTANCE instance, WNDPROC mainWinProc) {
		RegisterMainWindowClass(instance, mainWinProc);
	}

	inline void SetFullscreen(WindowHandle window, bool fullscreen, bool exclusive = false)
	{
		WindowRect rect;
		auto success = GetWindowRect(window, &rect);
		assert(success);
		if(fullscreen) // Switching to fullscreen.
		{
			// Set the window style to a borderless window so the client area fills
			// the entire screen.
			UINT windowStyle = WS_POPUP;

			SetWindowLongW(window, GWL_STYLE, windowStyle);

			// Query the name of the nearest display device for the window.
			// This is required to set the fullscreen dimensions of the window
			// when using a multi-monitor setup.
			const auto hMonitor = MonitorFromWindow(window, MONITOR_DEFAULTTONEAREST);
			MONITORINFOEX monitorInfo = {};
			monitorInfo.cbSize = sizeof(MONITORINFOEX);
			GetMonitorInfo(hMonitor, &monitorInfo);

			SetWindowPos(window, HWND_TOPMOST,
						 monitorInfo.rcMonitor.left,
						 monitorInfo.rcMonitor.top,
						 monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left,
						 monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top,
						 SWP_FRAMECHANGED | SWP_NOACTIVATE);

			ShowWindow(window, SW_MAXIMIZE);
		} else {
			// Restore all the window decorators.
			SetWindowLong(window, GWL_STYLE, WS_OVERLAPPEDWINDOW);

			SetWindowPos(window, HWND_NOTOPMOST,
						 rect.left,
						 rect.top,
						 rect.right - rect.left,
						 rect.bottom - rect.top,
						 SWP_FRAMECHANGED | SWP_NOACTIVATE);

			ShowWindow(window, SW_NORMAL);
		}
	}
}
