#include <string>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif // _WIN32



enum Severity {
	Info,
	Warning,
	Error
};

inline void Log(const char* msg, Severity level = Info) {
#ifdef _WIN32
	OutputDebugString(msg);
	OutputDebugString("\n");
#endif // _WIN32

}

inline void Log(std::string msg, Severity level = Info) {
	const char* buff = msg.c_str();
	Log(buff, level);
}

template <typename  T, typename std::enable_if<std::is_arithmetic<T>::value>::type* = nullptr>
inline void Log(T msg) {
	auto str = std::to_string(msg);
	Log(str);
}