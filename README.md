# Disclaimer
None of the below statements is yet fulfilled.
Don't expect a working game-engine.
This is all WIP.

# Nucleus
Nucleus is a modern, high performance game engine written in modern C++17. Using high level C#, developers can write games in a convenient language without having to worry about any heavy lifting.

## Features
- Various platforms supported
- Modern C# 8 support for scripting
- Support for modern graphics pipelines
- Outstanding performance and visuals
- Networking out of the box
- Community-driven

## Platforms
Nucleus runs on almost any modern OS. 
Builds can be run on following platforms
- Windows 7+
- UWP  
- MacOS
- Linux
- Playstation 4/5
- Xbox One /Series X
- Nintendo Switch

The editor used to work with the engine has builds available for the following platforms
- Windows 7+
- MacOS
- Linux

## Graphics 
Using modern rendering pipelines, the engine is capable of AAA visuals without suffering from performance issues.
Using either DX12 or Vulkan -Metal on iOS and MacOS -most modern hardware is supported without the need to provide workarounds or implement legacy pipelines for older devices which reduces performance. Modern features such as RTX, SMAA/TXAA, SSAO and Hairworks are all supported, depending on the render pipeline chosen.

## Networking
Nucleus featured ready-to-use networking. 
Using simple to use API calls developers can get their game running over multiple clients in just a few lines of code.


### Tech

Nucleus uses a number of frameworks and tech:
TBD
