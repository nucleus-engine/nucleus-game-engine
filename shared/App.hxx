#pragma once
#include <cstdint>
#include "../utils/Typedefs.hxx"
#include <chrono>

namespace Nucleus {
	class App {
	public:
		App() = default;
		virtual ~App() = default;
		virtual void Init(AppInstance instance, WindowHandle mainWindow) = 0;
		virtual void Start() = 0;
		virtual void Update() = 0;
		virtual void Stop() = 0;
		virtual void SendCommand(void* cmd) = 0;
		virtual void Resize(uint32_t width, uint32_t height) = 0;
		virtual void Minimize() = 0;
		virtual float TimeSinceStartup() = 0;
		virtual void SetMaxFPS(uint16_t fps) = 0;
		virtual uint16_t MaxFPS() = 0;

	protected:
		std::chrono::high_resolution_clock _clock;
		std::chrono::time_point<std::chrono::steady_clock> _startup = std::chrono::high_resolution_clock::now();
		WindowHandle _mainWindow;
		AppInstance _appInstance;
		bool _isFullscreen;
	};
}
